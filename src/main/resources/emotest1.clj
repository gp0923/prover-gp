{:name        "Nothing happened"
 :description "So this should be false"
 :assumptions {}
 :goal         (joy)}

{:name        "Im cool in the evening"
 :description "This should be true"
 :assumptions {1 (> noon morning)
               2 (> evening noon)
               3 (Desires! gavin evening (holds (CON rain gavin cool) noon))
               4 (Knows! gavin evening (happens rain morning))
               5 (Believes! gavin evening (implies (happens rain morning) (holds (CON rain gavin cool) noon)))}
 :goal         (joy)}

