package com.naveensundarg.shadow.prover.representations.formula;

/**
 * Indicates that a class represents a modal operator relating to a single agent.
 */
public interface SingleAgentModal extends HasAgent, HasTime, HasFormula {
}
