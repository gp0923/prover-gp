package com.naveensundarg.shadow.prover.representations.formula;

/**
 * Indicates that a class has been parametrized with a Formula.
 */
public interface HasFormula {

    Formula getFormula();
}
