package com.naveensundarg.shadow.prover.representations.formula;

import com.naveensundarg.shadow.prover.representations.value.Value;

/**
 * Indicates that a given formula relates to a specific agent.
 */
public interface HasAgent {

    Value getAgent();
}
