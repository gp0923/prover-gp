package com.naveensundarg.shadow.prover.representations.formula;

import com.naveensundarg.shadow.prover.representations.value.Value;

/**
 * Indicates that a formula relates to a specific time.
 */
public interface HasTime {

    Value getTime();
}
