package com.naveensundarg.shadow.prover.utils;

/**
 * Indicated a failure during the parsing process.
 */
public class ParsingException extends Exception {

    private String message;

    public ParsingException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
