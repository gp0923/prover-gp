package com.naveensundarg.shadow.prover.utils;

import us.bpsm.edn.parser.Parseable;
import us.bpsm.edn.parser.Parser;
import us.bpsm.edn.parser.Parsers;

/**
 * Pairs a Parseable with a Parser, and provides parsing utilities
 * to take the parsing configuration and setup away from code that
 * shouldn't be handling it.
 *
 * This class wraps around the external parsing library. No prover
 * logic or machinery is in here.
 */
public class ParseablePair {

    private static Parseable parseable;
    private static Parser parser;

    public ParseablePair(Parseable parseable, Parser parser) {
        this.parseable = parseable;
        this.parser = parser;
    }

    /**
     * @return The next value from the Parseable.
     */
    public Object nextValue() {
        return parser.nextValue(parseable);
    }

    public Parser getParser() {
        return parser;
    }

    public Parseable getParseable() {
        return parseable;
    }

    public static ParseablePair toParseablePair(CharSequence input) {
        Parseable parseable = Parsers.newParseable(input);
        Parser parser = Parsers.newParser(Parsers.defaultConfiguration());

        return new ParseablePair(parseable, parser);
    }

    public static ParseablePair toParseablePair(Readable input) {
        Parseable parseable = Parsers.newParseable(input);
        Parser parser = Parsers.newParser(Parsers.defaultConfiguration());

        return new ParseablePair(parseable, parser);
    }

    /**
     * @return The first parsed value from the input.
     */
    public static Object firstValue(CharSequence input) {
        return toParseablePair(input).nextValue();
    }

    /**
     * @return The first parsed value from the input.
     */
    public static Object firstValue(Readable input) {
        return toParseablePair(input).nextValue();
    }

}
