package com.logic.prover;

import com.naveensundarg.shadow.prover.core.CognitiveCalculusProver;
import com.naveensundarg.shadow.prover.core.Problem;
import com.naveensundarg.shadow.prover.core.Prover;
import com.naveensundarg.shadow.prover.core.proof.Justification;
import com.naveensundarg.shadow.prover.representations.formula.And;
import com.naveensundarg.shadow.prover.representations.formula.Formula;
import com.naveensundarg.shadow.prover.representations.formula.Not;
import com.naveensundarg.shadow.prover.representations.formula.Universal;
import com.naveensundarg.shadow.prover.representations.value.Variable;
import com.naveensundarg.shadow.prover.utils.ParseablePair;
import com.naveensundarg.shadow.prover.utils.ParsingException;
import com.naveensundarg.shadow.prover.utils.ProblemReader;
import com.naveensundarg.shadow.prover.utils.Reader;

import java.io.*;
import java.util.*;

/**
 * Given file names as argumments, this program will use the
 * cognitive calculus prover to attempt to prove that a
 * passed emotion is exhibited in the situation given by the passed assumptions.
 * Passed files should be correctly formatted.
 */
public class TestProof {

    public static void main(String[] args) {
        if (args.length < 3) {
            System.err.println("Incorrect number of arguments.");
            System.err.println("Format: <emotionsets.csv> <emotionformulae.clj> <problems.clj>");
            return;
        }
        //Initialize prover and structures for emotions
        Map<String, Formula> formulae;
        Map<String, String[]> emotions;

        //Initialize emotion maps
        formulae = getEmotionFormulae(args[1]);
        emotions = getEmotionSets(args[0]);

        //Loop through the files passed
        for (int i = 2; i < args.length; i++) {
            String filename = args[i];
            InputStream stream = null;
            List<Problem> problems;
            try {
                stream = new FileInputStream(filename);
                problems = ProblemReader.readFrom(stream);
                for (Problem problem : problems) {
                    Set<Variable> goals = problem.getGoal().variablesPresent();
                    if (goals.size() == 1){
                        String emotion = goals.iterator().next().toString();
                        emotion = emotion.trim();
                        boolean proven = proveEmotion(problem, emotion, emotions, formulae);
                        System.out.println("Name: " + problem.getName());
                        System.out.println("Description: " + problem.getDescription());
                        if (proven)
                            System.out.println("Proof found for " + emotion);
                        else
                            System.out.println("Proof not found for " + emotion);
                        System.out.println();
                    }
                    else if (goals.isEmpty()){
                        //Determine if any emotions in the list match the assumptions
                        System.err.println("Emotion selection from not implemented");
                    }else{
                        //Given a list of emotions, determine if any match the assumptions
                        System.err.println("Emotion selection from a list not implemented");
                    }

                }

            } catch (IOException | ParsingException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (stream != null)
                        stream.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Get a list of formula from a file.
     * The formula will be returned in a map with the problem names as keys.
     * Each formula should be the goal of a different Problem.
     *
     * @param filename the name of the file, ideally a .clj file.
     *                 The file should be formatted as a set of problems with
     *                 the formulae as goals.
     * @return a map of goal formulae with the problem names as keys.
     */
    private static Map<String, Formula> getEmotionFormulae(String filename) {
        FileInputStream stream = null;
        Map<String, Formula> map = null;
        try {
            stream = new FileInputStream(filename);
            map = Reader.readFormulaMap(ParseablePair.firstValue(new InputStreamReader(stream)));
        } catch (ParsingException | FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stream != null)
                    stream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return map;
    }

    /**
     * Get a map of emotions and their associated formulae combinations.
     *
     * @param filename the file containing the emotion names and
     *                 associated formulae sets. The file should be a .csv file and
     *                 the emotions should be in the format:
     *                 emotion1,axiom1 axiom2 ...
     *                 emotion2,axiom3 axiom4 ...
     *                 ...
     * @return a map containing Emotion names as strings mapped to a list of strings
     * representing axiom names.
     */
    private static Map<String, String[]> getEmotionSets(String filename) {
        String line;
        Map<String, String[]> emomap = new TreeMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {

            while ((line = br.readLine()) != null) {
                line = line.trim();

                if (line.isEmpty())
                    continue;
                String[] emotion = line.split(",");
                if (emotion.length != 2) {
                    System.err.println("ERROR: \"" + line + "\" is not formatted correctly.");
                    return null;
                }
                String[] axioms = emotion[1].split(" ");
                Arrays.sort(axioms);
                emomap.put(emotion[0], axioms);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return emomap;
    }

    /**
     * Prove that the passed formulae imply an emotion.
     *
     * @param problem  the problem to prove, only the assumptions are used
     * @param emoname  the name of the emotion to prove
     * @param emotions a map of emotions to their formula sets
     * @param formulae a map of formulae names to their formulae
     * @return true if the emotion can be proven, false otherwise
     */
    private static boolean proveEmotion(Problem problem, String emoname,
                                        Map<String, String[]> emotions,
                                        Map<String, Formula> formulae) {
        try {
            Reader.readFormulaFromString("(not (= ?a ?a1))");
        } catch (ParsingException e) {
            e.printStackTrace();
        }
        Set<Formula> assumptions = problem.getAssumptions();

        String[] emotionaxioms = emotions.get(emoname);
        if (emotionaxioms == null) {
            System.out.println("ERROR: Emotion \"" + emoname + "\" not found.");
            return false;
        }

        Prover prover = new CognitiveCalculusProver();
        ArrayList<Formula> axiomforms = new ArrayList<>(emotionaxioms.length);
        for (String axiom : emotionaxioms) {

            if (!formulae.containsKey(axiom)) {
                System.out.println("ERROR: Axiom \"" + axiom + "\" not found.");
                return false;
            }
            Formula axformula = formulae.get(axiom);
            if (!assumptions.contains(axformula))
                axiomforms.add(axformula);
        }
        if (axiomforms.isEmpty())
            return true;

        Formula axiomconjunction;
        if (axiomforms.size() > 1)
            axiomconjunction = new And(axiomforms);
        else
            axiomconjunction = axiomforms.get(0);
        Formula axiomexist = new Not(new Universal(axiomconjunction.variablesPresent().toArray(new Variable[0]), new Not(axiomconjunction)));
        //Formula axiomexist = axiomconjunction;

        System.out.println("DEBUG: Proving " + axiomexist + " with assumptions:\n" + assumptions);
        Optional<Justification> justification = prover.prove(assumptions, axiomconjunction);
        //Print the justification if present
        justification.ifPresent(justification1 -> System.out.println("DEBUG: Justification: " + justification1.toString() + "\n"));
        return justification.isPresent();
    }

    private static boolean determineEmotions(Problem problem, String emoname,
                                        Map<String, String[]> emotions,
                                        Map<String, Formula> formulae) {

        //Optional<Value> proveAndGetBinding(Set<Formula> assumptions, Formula formula, Variable variable)
        return false;
    }
}